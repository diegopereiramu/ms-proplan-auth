import { buscar as obtenerUsuario, roles as obtenerRolesUsuario } from  '@repository/users';
import { mapToBodyJwt } from '@helpers/utils';
import { obtener, registrar, expirar as expirarSesion } from  '@repository/session';
import { revocar } from  '@repository/jwt';

const jwt = require('njwt');

const validar = async (req, res, next) => {
    const session = req.body.session;
    const data = await obtener(req.body.session);
    if(data != null){
        try {
            jwt.verify(data, process.env.JWT_SECRET_KEY);    
            res.status(200).json({
                OK: true,
                data: {
                    token: data
                }
            });  
        } catch (error) {           
            if(error.message == 'Jwt is expired'){
                const user = await obtenerUsuario(error.parsedBody.user.id);   
                if(user.length === 1){
                    const roles = await obtenerRolesUsuario(user[0].id);
                    const token = jwt.create(mapToBodyJwt(user[0], roles), process.env.JWT_SECRET_KEY);
                    token.setExpiration(new Date().getTime() + process.env.JWT_EXPIRATION*1000);                    
                    try {
                        await registrar(session, token.compact());    
                        res.status(200).json({
                            OK: true,
                            data: {
                                token: token.compact()
                            }
                        });      
                    } catch (error) {
                        res.status(200).json({
                            OK: false,
                            data: null,
                            error: error
                        });         
                    }
                }
            }
        }        
    }else{
        res.status(200).json({
            OK: false,
            data: null,
            error: {
                codigo: "SESION_EXPIRADA"
            }
        }); 
    }
};

const expirar = async (req, res, next) => {
    const token = await obtener(req.body.session);
    if(token != null){
        await expirarSesion(req.body.session);
        await revocar(token);
    }
    res.status(200).json({
        OK: true,
        data: null
    });  
};
export {
    validar,
    expirar,
};