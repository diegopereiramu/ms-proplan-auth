import { buscar as obtenerUsuario, roles as obtenerRolesUsuario } from  '@repository/users';
import { mapToBodyJwt } from '@helpers/utils';

const jwt = require('njwt');

const refresh = async (req, res, next) => {
    try {
        const data = jwt.verify(req.body.token, process.env.JWT_SECRET_KEY);
        const user = await obtenerUsuario(data.body.user.id);   
        if(user.length === 1){
            const roles = await obtenerRolesUsuario(user[0].id);
            const token = jwt.create(mapToBodyJwt(user[0], roles), process.env.JWT_SECRET_KEY)
            token.setExpiration(new Date().getTime() + process.env.JWT_EXPIRATION*1000)   
            res.status(200).json({
                OK: true,
                data: token.compact(),
                error: null
            });
        }
    } catch (error) {
        res.status(200).json({
            OK: false,
            data: null,
            error: {
                descripcion: "Error refrescando JWT",
                detalle: error.message
            }
        });    
    }
};

export {
    refresh,
};