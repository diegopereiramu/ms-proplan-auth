import { auth as autenticarUsuario, roles as obtenerRolesUsuario,buscar } from  '@repository/users';
import { mapToBodyJwt, mapToSession } from '@helpers/utils';
import { registrar } from  '@repository/session';
import { auth as autenticarUsuarioWsdl} from '@services/auth';

const jwt = require('njwt');

const auth = async (req, res, next) => {
    let authOK = await autenticarUsuarioWsdl(req.body.user, req.body.pass);
    if(authOK){
        const session = mapToSession(req.body.user);
        const roles = await obtenerRolesUsuario(req.body.user);
        const userObj = await buscar(req.body.user)
        const token = jwt.create(mapToBodyJwt(userObj, roles), process.env.JWT_SECRET_KEY)
        token.setExpiration(new Date().getTime() + process.env.JWT_EXPIRATION*1000)
        try {
            await registrar(session, token.compact());
            res.status(200).json({
                OK: true,
                data: {
                    session: session,
                    user: userObj,
                    roles: roles
                }
            });
        } catch (error) {
            res.status(200).json({
                OK: false,
                data: null,
                error: error
            });
        }
    } else {
        res.status(200).json({
            OK: false,
            data: null
        });
    }
};

export {
    auth,
};
