import { revocado } from  '@repository/jwt';

const revoked = async (req, res, next) => {
    try {
        const blacklist = await revocado(req.body.token);
        res.status(200).json({
            OK: blacklist === 1 ? true : false,
            error: null
        });    
    } catch (error) {
        res.status(200).json({
            OK: false,
            data: null,
            error: error
        });    
    }
};

export {
    revoked,
};