import { revocar } from  '@repository/jwt';

const revoke = async (req, res, next) => {
    try {
        await revocar(req.body.token);
        res.status(200).json({
            OK: true,
            error: null
        });    
    } catch (error) {
        res.status(200).json({
            OK: false,
            data: null,
            error: error
        });    
    }
};

export {
    revoke,
};