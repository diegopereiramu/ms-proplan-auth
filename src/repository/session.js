import {setex, get, del} from './redis.connect';

const registrar = async (session, jwt) => {
    return await setex(process.env.REDIS_SESSION_NAMESPACE+session, process.env.REDIS_SESSION_TTL, jwt);
};

const obtener = async (session) => {
    return await get(process.env.REDIS_SESSION_NAMESPACE+session);
};

const expirar = async (session) => {
    return await del(process.env.REDIS_SESSION_NAMESPACE+session);
};

export {
    registrar,
    expirar,
    obtener,
}