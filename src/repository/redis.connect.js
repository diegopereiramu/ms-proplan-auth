const redis = require("redis");
const util = require('util');
const client = redis.createClient({
    host: process.env.REDIS_HOST,
    port: process.env.REDIS_PORT,
    password: process.env.REDIS_PASS,
    detect_buffers: true
});

const exists = util.promisify(client.exists).bind(client);
const get = util.promisify(client.get).bind(client);
const setex = util.promisify(client.setex).bind(client);
const del = util.promisify(client.del).bind(client);

export {exists, get, setex, del};
export default client;