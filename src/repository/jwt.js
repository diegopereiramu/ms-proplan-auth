import {exists, setex} from './redis.connect';

const revocar = async (jwt) => {
    return await setex(process.env.REDIS_JWT_NAMESPACE+jwt, process.env.REDIS_JWT_TTL, "");
};

const revocado = async (jwt) => {
    const blacklist = await exists(process.env.REDIS_JWT_NAMESPACE+jwt);
    return blacklist;
};

export {
    revocar,
    revocado,
}