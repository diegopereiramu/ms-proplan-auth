import { query } from './pg.connect';

const auth = async (usuario, pass) => {

    const q = `
    SELECT 
        id,
        nombre,
        usuario,
        activo
    FROM 
        "proplan"."usuario"
    WHERE
        usuario = '${usuario}' AND
        clave = md5('${pass}');`
    const result = await query(q);
    return result.rows;
};

const roles = async (usuario) => {
    const q = `
    select ur.rol_id as id, r.codigo
from "proplan"."usuario_rol" ur
inner join "proplan"."rol" r on r.id = ur.rol_id
inner join "proplan"."usuario" u on u.id = ur.usuario_id
where u.usuario like '${usuario}' and u.activo;`
    const result = await query(q);
    return result.rows;
};

const buscar = async (Usuario) => {
    try{
    const q = `
    SELECT 
        id,
        nombre,
        usuario,
        activo
    FROM 
        "proplan"."usuario"
    WHERE
        usuario = '${Usuario}';`
    const result = await query(q);
    return result.rows[0];
    }catch(e){
        console.log(e);
    }
};

export {
    auth,
    roles,
    buscar
}
