import { query } from './pg.connect';

const getDate = async (user, users) => {
    const result = await query(`SELECT now() as date;`);
    return result.rows[0].date;
};

export {
    getDate,
}
