import { auth } from '@actions/auth';
import { refresh } from '@actions/refresh';
import { revoke } from '@actions/revoke';
import { revoked } from '@actions/revoked';

export default (router, app) => {
    router.post('/', auth);
    router.put('/', refresh);
    router.delete('/token/blacklist', revoke);
    router.post('/token/blacklist', revoked);
};