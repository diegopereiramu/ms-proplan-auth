import { validar, expirar } from '@actions/session';

export default (router, app) => {
    router.post('/session/validate', validar);
    router.post('/session/expire', expirar);
};