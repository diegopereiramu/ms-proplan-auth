const soapRequest = require('easy-soap-request');
const parseString = require('xml2js').parseString;
var url = 'http://10.8.117.247/webservices/sac/usuario/index.php';


const auth = async (usuario, pass) => {
    const args = {usuario: usuario, clave:pass, vToken:process.env.AUTH_TOKEN};
    const sampleHeaders = {
        'Content-Type': 'text/xml;charset=UTF-8',
        'soapAction': 'http://10.8.117.247/webservices/sac/usuario?wsdl#ingreso',
    };
    const xml = `
        <soapenv:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:urn="urn:wsdl">
           <soapenv:Header/>
           <soapenv:Body>
              <urn:ingreso soapenv:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/">
                 <Entrada xsi:type="urn:Entrada">
                    <usuario xsi:type="xsd:string">${args.usuario}</usuario>
                    <password xsi:type="xsd:string">${args.clave}</password>
                    <vToken xsi:type="xsd:string">${args.vToken}</vToken>
                 </Entrada>
              </urn:ingreso>
           </soapenv:Body>
        </soapenv:Envelope>`;

        const {response} = await soapRequest({url: url, headers: sampleHeaders, xml: xml, timeout: 1000});
        var authOK = 0;
        parseString(response.body, (err, result) => {
            authOK = result["SOAP-ENV:Envelope"]["SOAP-ENV:Body"][0]["ns1:ingresoResponse"][0]["return"][0]["respuesta"][0]["_"]==="1";
        });
        return authOK;
}

export {
    auth
};
