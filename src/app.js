import api from '@amacl/roboc-api';
import { apiOptions } from '@config';
import auth from '@routes/auth';
import session from '@routes/session';

const run = () => {
    api(apiOptions,(router, app) => {
        auth(router);
        session(router);
        return router;
    });
};

export {
    run,
}