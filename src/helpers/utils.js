const crypto = require('crypto');

const mapToBodyJwt = (user, roles) => {
    return { 
        iss: 'proplan', 
        sub: 'ssbiobio', 
        user: user,
        roles: roles
    };
};

const mapToSession = (userId) => {
    const data = userId.toString()+';'+new Date().getTime();
    const session = crypto.createHash('sha256').update(data).digest("hex");
    return session;
};

export {
    mapToBodyJwt,
    mapToSession
};