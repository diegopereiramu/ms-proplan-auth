# Microservicio Template

## Clonar

> Ajustar estp para tu REAME

```sh
$ git clone https://bitbucket.org/amarischile/ms-proplan-template.git ms-proplan-nombre-de-tu-servicio
$ cd ms-proplan-nombre-de-tu-servicio
$ cp .env .env.local
$ npm install
$ npm test
$ npm run start:local
```
> Debes configurar las variables de entorno para tu desarrollo local en .env.local


## Actualizar

### Modificar package.json

* Cambiar ms-name por el nombre de tu servicio

por ejemplo:

> Si tu servicio es ms-proplan-profesionales >> @proplan/profesionales

* {repository} cambiar por tu servicio

```json
{
    "name": "@proplan/ms-name",
    ...
    "description": "",
    ...
    "repository": {
      "type": "git",
      "url": "git+ssh://git@bitbucket.org/amarischile/{repository}.git"
    },
    ...
    "homepage": "https://bitbucket.org/amarischile/{repository}#README"
    ...
}
```

### Desanclar repositorio template

* {repository} debe ser el nombre de tu repositiorio.

```sh
$ rm -rf .git
$ git init
$ git remote add origin https://bitbucket.org/amarischile/{repository}.git
$ git add .
$ git commit --m [ftr] init
$ git push origin master
```

## Docker

* {repository} debe ser el nombre de tu repositiorio.

```sh
$ docker build -t {repository} .

$ docker run -it \
-e APP_PORT=1000 \
-e APP_HOST=0.0.0.0 \
-e APP_PREFIX=/ \
-e APP_HEALTH_PATH=/health \
-e MYSQL_CONNECTION_STRING="mysql://user:pass@server/dbname?debug=true&charset=UTF8_GENERAL_CI" \
-p 1000:1000 --name {repository} {repository}
```

## Verificar

Verificar que el servicio este arriba

```sh
$ curl -v http://localhost:1000/health
```

```sh
$ curl -v http://localhost:1000/test-db
```

## NOTA

> Actualizar README asociado al nuevo microservicio
> Quita las partes que no son necesarias